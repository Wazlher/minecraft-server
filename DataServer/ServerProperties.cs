﻿using System;
using System.Net;

namespace DataServer
{
    public class ServerProperties
    {
        string path;
        ServerPropertiesParser parser;

        public int port = 25565;
        public string ip = "";
        public byte gamemode = 0;

        public ServerProperties(string _path)
        {
            path = _path;
            parser = new ServerPropertiesParser(path);
        }

        public bool ParsePropertiesFile()
        {
            if (parser.Parse())
            {
                port = int.Parse(parser.GetValue("port", port.ToString()));
                ip = GetIP();
                gamemode = byte.Parse(parser.GetValue("gamemode", gamemode.ToString()));
            }
            else
                return false;
            return true;
        }

        private string GetIP()
        {
            string addr = parser.GetValue("ip", ip);
            if (addr == string.Empty)
            {
                IPHostEntry host;
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ipa in host.AddressList)
                {
                    if (ipa.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        addr = ipa.ToString();
                        break;
                    }
                }
            }
            return addr;
        }

        public static string LongToIP(long longIP)
        {
            string ip = string.Empty;
            for (int i = 0; i < 4; i++)
            {
                int num = (int)(longIP / Math.Pow(256, (3 - i)));
                longIP = longIP - (long)(num * Math.Pow(256, (3 - i)));
                if (i == 0)
                    ip = num.ToString();
                else
                    ip = ip + "." + num.ToString();
            }
            return ip;
        }

        public static long IPToLong(string ip)
        {
            string[] ipBytes;
            double num = 0;
            char[] charArray = ip.ToCharArray();
            Array.Reverse(charArray);
            ip = new string(charArray);
            if (!string.IsNullOrEmpty(ip))
            {
                ipBytes = ip.Split('.');
                for (int i = ipBytes.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(ipBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                }
            }
            return (long)num;
        }
    }
}
