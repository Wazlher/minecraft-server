﻿namespace DataServer
{
    public class Player : Entity
    {
        /* Game data */
        public string name;
        public bool OnTheGround;
        public float ExperienceBar = 0.0f;
        public short Level = 1;
        public short TotalExperience = 0;
        public float speedWalk;
        public float speedFly;
        public short nbSlot;
        public float health;
        public short food;
        public float foodSaturation;
        public Vector3 spawnPosition;
        public double headPosY;
        public float rotX;
        public float rotY;

        /* Constructors */
        public Player(int _id) : base(_id)
        {
            name = "NoName";
            spawnPosition = new Vector3(0.0, 100.0, 0.0);
            position = spawnPosition;
            headPosY = position.y + 1.62;
            OnTheGround = true;
            speedFly = 0.1f;
            speedWalk = 1.0f;
            health = 10.0f;
            food = 0;
            foodSaturation = 0;
        }
    }
}
