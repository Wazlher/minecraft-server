﻿namespace DataServer
{
    public class Vector3
    {
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }

        public Vector3(double _x, double _y, double _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }

        public Vector3(Vector3 vec)
        {
            x = vec.x;
            y = vec.y;
            z = vec.z;
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            v1.x += v2.x;
            v1.y += v2.y;
            v1.z += v2.z;
            return new Vector3(v1);
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            v1.x -= v2.x;
            v1.y -= v2.y;
            v1.z -= v2.z;
            return new Vector3(v1);
        }

        public static bool operator !=(Vector3 v1, Vector3 v2)
        {
            if (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z)
                return false;
            return true;
        }

        public static bool operator ==(Vector3 v1, Vector3 v2)
        {
            if (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z)
                return true;
            return false;
        }
    }
}
