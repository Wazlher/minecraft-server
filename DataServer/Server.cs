﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace DataServer
{
	public class Server
	{
		public enum ServerState
		{
			E_NOT_STARTED,
			E_RUNNING,
		}

		public event Action<Client> ClientChange;

		private TcpListener listener;
		public ClientManager clientManager { get; private set; }
		public List<Entity> entityList;
		public List<Chunk> chunkList;

		private Dictionary<Task, Client> tasks = new Dictionary<Task, Client>();
		public ServerState state;
		public ServerProperties properties { get; private set; }
		private System.Timers.Timer timeEntities;

		public Server()
		{
			state = ServerState.E_NOT_STARTED;
			clientManager = new ClientManager(this);
			entityList = new List<Entity>();
			timeEntities = new System.Timers.Timer(50);
			timeEntities.Elapsed += ManageHandleEntities;
			timeEntities.Start();
		}

		public void initChunk()
		{
			Debug.WriteLine("InitChunk");
			chunkList = new List<Chunk>();

			for (int y = -6; y < 6; y++)
			{
				for (int x = -6; x < 6; x++)
				{
					Chunk clientChunk = new Chunk();
					clientChunk.chunkData(y, x);
					clientChunk.initMapFormat(1);
					chunkList.Add(clientChunk);
				}
			}
		}

		public bool Init(string pathname)
		{
			properties = new ServerProperties(pathname);
			if (!properties.ParsePropertiesFile())
				return false;

			Debug.WriteLine("[INFO] Serveur init OK");

			initChunk();
			return true;
		}

		public bool Start()
		{
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(properties.ip).Address, properties.port);


			/* Peut etre try/catch ici */

			listener = new TcpListener(endPoint);
			listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
			listener.Start();
			state = ServerState.E_RUNNING;
			ThreadPool.QueueUserWorkItem(new WaitCallback(Run));
			return true;
		}

		public void Run(object st)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveAndSend));

			while (state == ServerState.E_RUNNING)
			{
				TcpClient client = listener.AcceptTcpClient();
				Client cl = clientManager.AddClient(client);
				ClientChange(cl);
			}
		}

		private void ReceiveAndSend(object state)
		{
			while (true)
			{
				while (tasks.Count != 0)
				{
					foreach (var t in tasks)
					{
						if (t.Key.IsCanceled && t.Key.IsFaulted)
							t.Key.Start();
						else if (t.Key.IsCompleted)
						{
							ClientChange(t.Value);
							tasks.Remove(t.Key);
							break;
						}
					}
				}

				List<Client> cl = clientManager.clientList;

				for (int i = 0; i < cl.Count; ++i)
				{
                    try
                    {
                        if (cl[i].stream.CanRead && cl[i].stream.DataAvailable)
                        {
                            BytesBuffer packet = new BytesBuffer();
                            packet.Capacity = 1024;
                            var bytesRead = cl[i].stream.Read(packet.bytes, 0, packet.bytes.Length);
                            packet.bytesRead = bytesRead;
                            //	Debug.WriteLine("bytes read: " + bytesRead);
                            packet.Write(packet.bytes, 0, packet.bytes.Length);
                            packet.Position = 0;
                            cl[i].parser.Add(packet);
                            Task t = new Task(cl[i].command.HandleCommand);
                            tasks.Add(t, cl[i]);
                            t.Start();
                        }
                    }
                    catch (Exception e)
                    {
                        break;
                    }
				}
			}
		}

		private void ManageHandleEntities(object sender, System.Timers.ElapsedEventArgs args)
		{
			if (state == ServerState.E_RUNNING && clientManager.clientList.Count > 0 && entityList.Count > 0)
			{
				Task k = new Task(clientManager.clientList[0].command.HandleEntities);
				k.Start();
			}
		}

		public bool Close()
		{
			List<Client> tmp = clientManager.RemoveAllUsers();
			foreach (Client cl in tmp)
				ClientChange(cl);
			state = ServerState.E_NOT_STARTED;
			return true;
		}

		public bool KickAClient(int id, bool stillConnected)
		{
			Client tmp = clientManager.KickAClient(id, stillConnected);
			if (tmp == null)
				return false;
			ClientChange(tmp);
			tmp = null;
			return true;
		}
	}
}
