﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Timers;

namespace DataServer
{
    public class Client
    {
        public TcpClient tcpClient;
        public NetworkStream stream;
        public CommandManager command;
        public ClientManager clientManager;
        public List<Client> toSpawn;

        public List<BytesBuffer> parser;
		public BytesBuffer buffer = new BytesBuffer();
        public int idByteArray;
        public int id;
        public Player player;
        public bool toRemove;
        public int nextState;
        public bool incomplete;

        public bool keepAlive;

        public List<Vector3> poss = new List<Vector3>();

        Timer myTimer = new Timer(3000);

        public Client(TcpClient _tcpClient, int _id, ClientManager _clientManager)
        {
            tcpClient = _tcpClient;
            parser = new List<BytesBuffer>();
            command = new CommandManager(this);
            stream = tcpClient.GetStream();
            id = _id;
            idByteArray = 0;
            player = new Player(id);
            toSpawn = new List<Client>();
            clientManager = _clientManager;
            keepAlive = false;

            myTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            myTimer.Start();
        }

        ~Client()
        {
            command = null;
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            int posX = (int)Math.Round(player.position.x / 16);
            int posZ = (int)Math.Round(player.position.z / 16);

            List<KeyValuePair<int, int>> chunkNeeded;
            chunkNeeded = new List<KeyValuePair<int,int>>();

            List<KeyValuePair<int, int>> chunkMissing;
            chunkMissing = new List<KeyValuePair<int, int>>();

            for (int x = posX - 6; x < posX + 6; x++)
            {
                for (int z = posZ - 6; z < posZ + 6; z++)
                {
                    chunkNeeded.Add(new KeyValuePair<int, int>(x, z));
                }
            }

            List<Chunk> listChunk;
            listChunk = clientManager.server.chunkList;

            bool find = false;

            foreach (KeyValuePair<int, int> j in chunkNeeded)
            {
                foreach (Chunk c in listChunk)
                {
                    if (j.Key == c.x && j.Value == c.z)
                    {
                        find = true;
                        break;
                    }
                    
                }
                if (find == false)
                {
                    chunkMissing.Add(new KeyValuePair<int, int>(j.Key, j.Value));
                }
            }

            foreach (KeyValuePair<int, int> j in chunkMissing)
            {
                Chunk clientChunk = new Chunk();
                clientChunk.chunkData(j.Key, j.Value);
                clientChunk.initMapFormat(15);
                clientManager.server.chunkList.Add(clientChunk);
                clientChunk.compressTableAndAdd(this);
            }
        }

        public void Close()
        {
            stream.Close();
            tcpClient.Close();
        }

       /* public void Send()
        {
            Byte[] arr = parser.ToArray();
            stream.Write(arr, 0, arr.Length);
        }*/

        public bool isConnected()
        {
            if (tcpClient != null && tcpClient.Client != null && tcpClient.Client.Connected)
            {
                if (tcpClient.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buff = new byte[1];
                    if (tcpClient.Client.Receive(buff, SocketFlags.Peek) == 0)
                        return false;
                    return true;
                }
            }
            return true;
        }
    }
}
