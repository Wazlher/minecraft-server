﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;

namespace DataServer
{
    public class CommandManager
    {
        public int lenght = 0;
        private Client client;

        private Timer keepAliveCheck;

        private Timer myTimer = new Timer(5000);

        public CommandManager(Client cl)
        {
            client = cl;
            myTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            myTimer.Start();
            keepAliveCheck = new Timer(4000);
            keepAliveCheck.AutoReset = false;
            keepAliveCheck.Elapsed += new ElapsedEventHandler(CheckKeepAlive);
        }

        ~CommandManager()
        {
            keepAliveCheck.Stop();
            myTimer.Stop();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (client.nextState == 3 && client.keepAlive == false)
            {
                HandleAlive(client);
                client.keepAlive = true;
                keepAliveCheck.Start();
            }
            else
                CheckKeepAlive();

        }

        private void CheckKeepAlive()
        {
            if (client.keepAlive == true)
                client.clientManager.server.KickAClient(client.id, false);
        }

        private void CheckKeepAlive(object sender, ElapsedEventArgs args)
        {
            if (client.keepAlive == true)
                client.clientManager.server.KickAClient(client.id, false);
        }

        public void HandleCommand()
        {
            while (client.toSpawn.Count != 0)
            {
                foreach (Client spawn in client.toSpawn)
                {
                    HandleSpawnPlayer(client, spawn);
                    client.toSpawn.Remove(spawn);
                    break;
                }
            }

            //Debug.WriteLine("Je suis dans HandleCommand");
            BytesBuffer currentBuffer = client.parser[0];
            client.parser.Remove(client.parser[0]);
            while (currentBuffer.Position < currentBuffer.bytesRead)
            {
                int commandId;
                int lenghtPacket;
                int rest = (int)currentBuffer.Length - (int)currentBuffer.Position;
                try
                {
                    lenghtPacket = currentBuffer.ReadVarInt();
                    //Debug.WriteLine("length: " + lenghtPacket);
                    commandId = currentBuffer.ReadVarInt();
                }
                catch (ArgumentOutOfRangeException e)
                {
                    BytesBuffer tmp = new BytesBuffer();

                    tmp.lenght = rest; ;
                    tmp.Write(currentBuffer.GetBuffer(), 0, (int)currentBuffer.Length);
                    client.parser.Add(tmp);
                    client.incomplete = true;
                    return;
                }
                if (client.nextState == 0)
                {
                    if (commandId == 0)
                    {
                        HandleHandshake(client, currentBuffer);
                        continue;
                    }
                }
                if (client.nextState == 1)
                {
                    if (commandId == 0)
                    {
                        HandleResponse(client);
                        continue;
                    }
                    if (commandId == 0x01)
                    {
                        Debug.WriteLine("LOL");
                        HandlePing(client, currentBuffer);
                        continue;
                    }
                }
                if (client.nextState == 2)
                {
                    if (commandId == 0)
                    {
                        HandleLoginStart(client, currentBuffer);
                        HandleLoginSuccess(client);
                        client.clientManager.server.entityList.Add(client.player);

                        HandleJoinGame(client, client.id);
                        HandleSpawnPosition(client);
                        HandlePlayerAbilities(client);

                        HandleItem(client);
                        HandleExperience(client);
                        HandleUpdateHealth(client);
                        HandleChunk(client);
                        HandlePositionAndLook(client);

                        //Spawn current player in each client
                        List<Client> clientList = client.clientManager.clientList;
                        foreach (Client cl in clientList)
                        {
                            if (cl.nextState == 3 && cl.id != client.id)
                            {
                                HandleSpawnPlayer(cl, client);
                                Debug.WriteLine("Client " + cl.id + " a rendu visible le client " + client.id);
                            }
                        }

                        // Spawn each player in current client
                        foreach (Client c in client.clientManager.clientList)
                        {
                            if (c != client && c.nextState == 3)
                            {
                                client.toSpawn.Add(c);
                                Debug.WriteLine("Client " + client.id + " a rendu visible le client " + c.id);
                            }
                        }
                        continue;
                    }
                }
                if (client.nextState == 3)
                {
                    if (commandId == 0x00)
                    {
                        Debug.WriteLine("[COMMAND] keep alive");
                        currentBuffer.ReadInt();
                        client.keepAlive = false;
                        continue;
                    }
                    if (commandId == 0x01)
                    {
                        Debug.WriteLine("[COMMAND] message");
                        currentBuffer.ReadUTF8String();
                        continue;
                    }
                    if (commandId == 0x02)
                    {
                        Debug.WriteLine("[COMMAND] use entity");
                        currentBuffer.ReadInt();
                        currentBuffer.ReadByte();
                        continue;
                    }
                    if (commandId == 0x03)
                    {
                        Debug.WriteLine("[COMMAND] on the ground");
                        currentBuffer.ReadBool();
                        continue;
                    }
                    if (commandId == 0x04)
                    {
                        HandlePlayerPosition(currentBuffer);
                        continue;
                    }
                    if (commandId == 0x05)
                    {
                        Debug.WriteLine("[COMMAND] player look");
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadBool();
                        continue;
                    }
                    if (commandId == 0x06)
                    {
                        Debug.WriteLine("[COMMAND] player position and look");
                        currentBuffer.ReadDouble();
                        currentBuffer.ReadDouble();
                        currentBuffer.ReadDouble();
                        currentBuffer.ReadDouble();
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadBool();
                        continue;
                    }
                    if (commandId == 0x07)
                    {
                        HandlePlayerDigging(client, currentBuffer);
                        continue;
                    }
                    if (commandId == 0x08)
                    {
                        HandlePlayerBlockPlacement(client, currentBuffer);
                        continue;
                    }
                    if (commandId == 0x09)
                    {
                        Debug.WriteLine("[COMMAND] item change");
                        currentBuffer.ReadShort();
                        continue;
                    }
                    if (commandId == 0x0A)
                    {
                        Debug.WriteLine("[COMMAND] animation");
                        currentBuffer.ReadInt();
                        currentBuffer.ReadByte();
                        continue;
                    }
                    if (commandId == 0x0B)
                    {
                        Debug.WriteLine("[COMMAND] entity action");
                        currentBuffer.ReadInt();
                        currentBuffer.ReadByte();
                        currentBuffer.ReadInt();
                        continue;
                    }
                    if (commandId == 0x0C)
                    {
                        Debug.WriteLine("[COMMAND] Vehicle");
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadBool();
                        currentBuffer.ReadBool();
                        continue;
                    }
                    if (commandId == 0x0D)
                    {
                        Debug.WriteLine("[COMMAND] close Window");
                        currentBuffer.ReadByte();
                        continue;
                    }
                    if (commandId == 0x0E)
                    {
                        Debug.WriteLine("[COMMAND] Click Window");
                        currentBuffer.ReadByte();
                        currentBuffer.ReadShort();
                        currentBuffer.ReadByte();
                        currentBuffer.ReadShort();
                        currentBuffer.ReadByte();
                        ReadSlot(currentBuffer);
                        continue;
                    }
                    if (commandId == 0x0F)
                    {
                        Debug.WriteLine("[COMMAND] Transaction");
                        currentBuffer.ReadByte();
                        currentBuffer.ReadShort();
                        currentBuffer.ReadBool();
                        continue;
                    }
                    if (commandId == 0x10)
                    {
                        Debug.WriteLine("[COMMAND] Create Inventory");
                        currentBuffer.ReadShort();
                        ReadSlot(currentBuffer);
                        continue;
                    }
                    if (commandId == 0x11)
                    {
                        Debug.WriteLine("[COMMAND] enchant item");
                        currentBuffer.ReadByte();
                        currentBuffer.ReadByte();
                        continue;
                    }
                    if (commandId == 0x12)
                    {
                        Debug.WriteLine("[COMMAND] Update Sign");
                        currentBuffer.ReadInt();
                        currentBuffer.ReadShort();
                        currentBuffer.ReadInt();
                        currentBuffer.ReadUTF8String();
                        currentBuffer.ReadUTF8String();
                        currentBuffer.ReadUTF8String();
                        currentBuffer.ReadUTF8String();
                        continue;
                    }
                    if (commandId == 0x13)
                    {
                        Debug.WriteLine("[COMMAND] player abilities");
                        currentBuffer.ReadByte();
                        currentBuffer.ReadFloat();
                        currentBuffer.ReadFloat();
                        continue;
                    }
                    if (commandId == 0x14)
                    {
                        Debug.WriteLine("[COMMAND] Tab complete");
                        currentBuffer.ReadUTF8String();
                        continue;
                    }
                    if (commandId == 0x15)
                    {
                        Debug.WriteLine("[COMMAND] client setting");
                        currentBuffer.ReadUTF8String();
                        currentBuffer.ReadByte();
                        currentBuffer.ReadByte();
                        currentBuffer.ReadBool();
                        currentBuffer.ReadByte();
                        currentBuffer.ReadBool();
                        continue;
                    }
                    if (commandId == 0x16)
                    {
                        Debug.WriteLine("[COMMAND] Client status");
                        currentBuffer.ReadByte();
                        continue;
                    }
                    if (commandId == 0x17)
                    {
                        Debug.WriteLine("[COMMAND] plugins message");
                        currentBuffer.ReadUTF8String();
                        short i = currentBuffer.ReadShort();
                        for (int j = 0; j < i; j++)
                            currentBuffer.ReadByte();
                        continue;
                    }
                    Debug.WriteLine("id: " + commandId);
                }
                /*       if (commandId == 0x00)
                           client.keepAlive = false;
                       if (commandId == 0x04)
                       {
                           HandlePlayerPosition(currentBuffer);
                           continue;
   =======
                           if (commandId == 0)
                           {
                               HandleResponse(client);
                               continue;
                           }
                           if (commandId == 0x01)
                           {
                               HandlePing(client, currentBuffer);
                               continue;
                           }
   >>>>>>> Everything is awesome
                       }
                       if (commandId == 0x07)
                       {
                           HandlePlayerDigging(client, currentBuffer);
                           continue;
                       }
                       if (commandId == 0x13)
                       {
   <<<<<<< HEAD
                           Debug.WriteLine("[DISCONNET] ?");
                       }
                       if (commandId == 0x08 && client.nextState == 3)
                       {
                           HandlePlayerBlockPlacement(client, currentBuffer);
                           continue;
                       }
                   }
               }*/
                client.parser.Clear();
                /*if (commandId == 0x2A)
                {
                    Debug.WriteLine("[COMMAND] Particle");
                    currentBuffer.ReadUTF8String();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadInt();
                    continue;
                }*/
                /*if (commandId == 0x27)
                {
                    Debug.WriteLine("[COMMAND] explosion");
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    int count = currentBuffer.ReadInt();
                    for (int i = 0; i < count; i++)
                    {
                        currentBuffer.ReadByte();
                        currentBuffer.ReadByte();
                        currentBuffer.ReadByte();
                    }
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    currentBuffer.ReadFloat();
                    continue;
                }*/
                /*if (commandId == 0x3F)
                {
                    Debug.WriteLine("[COMMAND] plugin message");
                    currentBuffer.ReadUTF8String();
                    short i = currentBuffer.ReadShort();
                    for (int j = 0; j < i; j++)
                        currentBuffer.ReadByte();
                    continue;
                }*/
                /*if (commandId == 0x3B)
                {
                    Debug.WriteLine("[COMMAND] scoreboard objective");
                    currentBuffer.ReadUTF8String();
                    currentBuffer.ReadUTF8String();
                    currentBuffer.ReadByte();
                    continue;
                }*/

                //       HandleEntities();
                /*      while (client.toSpawn.Count != 0)
                      {
                          foreach (Client spawn in client.toSpawn)
                          {
                              HandleSpawnPlayer(spawn);
                              client.toSpawn.Remove(spawn);
                              break;
                          }
                      }*/
                //client.parser.Clear();
            }
        }


        public void HandleAlive(Client client)
        {
            Random rnd = new Random();

            int id = rnd.Next(0, 10);

            BytesBuffer tmp = new BytesBuffer();
            BytesBuffer send = new BytesBuffer();

            tmp.WriteVarInt(0);
            tmp.WriteInt(id);

            send.WriteVarInt((int)tmp.Length);
            send.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            try
            {
                client.stream.Write(send.GetBuffer(), 0, (int)send.Length);
            }
            catch (System.ObjectDisposedException e)
            {
                return;
            }
        }
        //            HandleEntities();

        public void HandleResponse(Client client)
        {
            //Debug.WriteLine("[COMMAND] Response");

            BytesBuffer tmp = new BytesBuffer();
            BytesBuffer send = new BytesBuffer();

            tmp.WriteVarInt(0);
            tmp.WriteUTF8String("{\"version\":{\"name\":\"1.7.4\",\"protocol\":4},\"players\":{" +
                      "\"max\":30,\"online\":" + GetNbClientConnected(client.clientManager.clientList) + ",\"sample\":[]}," +
                      "\"description\":{\"text\":\"Exalike Team\"}" +
                      "}");

            send.WriteVarInt((int)tmp.Length);// lenght of packet
            send.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(send.GetBuffer(), 0, (int)send.Length);
        }

        private int GetNbClientConnected(List<Client> clientList)
        {
            int i = 0;

            foreach (Client cl in clientList)
            {
                if (cl.nextState == 2 || cl.nextState == 3)
                    ++i;
            }
            return i;
        }

        public void HandlePing(Client client, BytesBuffer buffer)
        {
            Debug.WriteLine("[COMMAND] Ping");
            long time = buffer.ReadLong();

            BytesBuffer tmp = new BytesBuffer();
            BytesBuffer send = new BytesBuffer();

            tmp.WriteVarInt(1);
            tmp.WriteLong(time);
            send.WriteVarInt((int)tmp.Length);// lenght of packet
            send.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(send.GetBuffer(), 0, (int)send.Length);
        }

        public void HandleHandshake(Client client, BytesBuffer currentBuffer)
        {
            Debug.WriteLine("[COMMAND] Handskake");
            int protocol = currentBuffer.ReadVarInt();
            string adress = currentBuffer.ReadUTF8String();
            short port = currentBuffer.ReadShort();
            client.nextState = currentBuffer.ReadVarInt();
            /* nextState = 1 when Minecraft is searching for server
               nextState = 2 when Minecraft want to join server */
            //Debug.Write("[DEBUG] Socket " + client.id + " , nextState = " + client.nextState);
        }

        public void HandleLoginStart(Client client, BytesBuffer currentBuffer)
        {
            Debug.WriteLine("[COMMAND] LoginStart");
            client.player.name = currentBuffer.ReadUTF8String();
        }

        public void HandleLoginSuccess(Client client)
        {
            //Debug.WriteLine("[COMMAND] LoginSuccess");
            BytesBuffer tmp = new BytesBuffer();
            string uuid = client.id.ToString();

            tmp.WriteVarInt(0x02);
            tmp.WriteUTF8String(uuid);
            tmp.WriteUTF8String(client.player.name);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);

            client.nextState = 3;
        }

        public void HandleJoinGame(Client client, int id)
        {
            //Debug.WriteLine("[COMMAND] JoinGame");

            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x01); //id of packet
            tmp.WriteInt(client.id);
            tmp.WriteByte(client.clientManager.server.properties.gamemode); // Mode de jeu - recuperer dans server properties
            tmp.WriteByte(0);// Dimension - recuperer dans server properties
            tmp.WriteByte(1);// Difficulte - recuperer dans server properties
            tmp.WriteByte(30);
            tmp.WriteUTF8String("default"); // Type de generation - recuperer dans server properties

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public void HandleSpawnPosition(Client client)
        {
            //Debug.WriteLine("[COMMAND] SpwanPosition");

            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x05); //id of packet
            tmp.WriteInt((int)client.player.spawnPosition.x);
            tmp.WriteInt((int)client.player.spawnPosition.y);
            tmp.WriteInt((int)client.player.spawnPosition.z);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public void HandlePlayerAbilities(Client client)
        {
            //Debug.WriteLine("[COMMAND] PlayerAbilities");

            byte flag = 0x01 | 0x02 | 0x04 | 0x08;

            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x39); //id of packet
            tmp.WriteByte(flag);
            tmp.WriteFloat(client.player.speedFly);
            tmp.WriteFloat(client.player.speedWalk);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        /*
         * Missing Player command 
         * */

        public void HandleItem(Client client)
        {
            //Debug.WriteLine("[COMMAND] HandleItem");
            BytesBuffer tmp = new BytesBuffer();
            short[] slot = new short[20];

            for (int i = 0; i < 20; i++)
                slot[i] = -1;

            tmp.WriteVarInt(0x30); //id of packet
            tmp.WriteByte(0); // ID de l'inventaire, 0 pour celui du joueur
            tmp.WriteShort(20);
            tmp.WriteShortArray(slot);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public void HandleExperience(Client client)
        {
            //Debug.WriteLine("[COMMAND] Experience");

            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x1F);
            tmp.WriteFloat(client.player.ExperienceBar);
            tmp.WriteShort(client.player.Level);
            tmp.WriteShort(client.player.TotalExperience);

            /*if (client.player.ExperienceBar >= 1)
            {
                client.player.Level++;
                float rest = client.player.ExperienceBar - 1;
                client.player.ExperienceBar = rest;
            }*/

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public void HandleUpdateHealth(Client client)
        {
            //Debug.WriteLine("[COMMAND] Update Health");
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x06);
            tmp.WriteFloat(client.player.health);
            tmp.WriteShort(client.player.food);
            tmp.WriteFloat(client.player.foodSaturation);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public void HandleChunk(Client client)
        {
            foreach (Chunk c in client.clientManager.server.chunkList)
                c.compressTableAndAdd(client);
        }

        public void HandlePositionAndLook(Client client)
        {
            Debug.WriteLine("[COMMAND] Position and look");
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x08);
            tmp.WriteDouble(0.0);
            tmp.WriteDouble(100.0);
            tmp.WriteDouble(0.0);
            tmp.WriteFloat(0.0f);
            tmp.WriteFloat(0.0f);
            tmp.WriteBool(true);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public void HandlePlayerPosition(BytesBuffer currentBuffer)
        {
            Debug.WriteLine("[COMMAND] PlayerPosition");
            client.player.prevPosition.x = client.player.position.x;
            client.player.prevPosition.y = client.player.position.y;
            client.player.prevPosition.z = client.player.position.z;
            client.player.position.x = currentBuffer.ReadDouble();
            client.player.position.y = currentBuffer.ReadDouble();
            client.player.headPosY = currentBuffer.ReadDouble();
            client.player.position.z = currentBuffer.ReadDouble();
            client.player.OnTheGround = currentBuffer.ReadBool();
            Debug.WriteLine("Client " + client.id + " PrevPOSX = " + client.player.prevPosition.x + " PrevPOSY = " +
                            client.player.prevPosition.y + " PrevPOSZ = " + client.player.prevPosition.z + " PosX = " +
                            client.player.position.x + " PosY = " + client.player.position.y + " PosZ = " + client.player.position.z);
        }

        public void HandlePlayerLook(BytesBuffer currentBuffer)
        {
            //Debug.WriteLine("[COMMAND] PlayerLook");
            client.player.rotX = currentBuffer.ReadFloat();
            client.player.rotY = currentBuffer.ReadFloat();
            client.player.OnTheGround = currentBuffer.ReadBool();
        }

        public void HandlePositionAndLook(BytesBuffer currentBuffer)
        {
            client.player.position.x = currentBuffer.ReadDouble();
            client.player.position.y = currentBuffer.ReadDouble();
            client.player.headPosY = currentBuffer.ReadDouble();
            client.player.position.z = currentBuffer.ReadDouble();
            client.player.rotX = currentBuffer.ReadFloat();
            client.player.rotY = currentBuffer.ReadFloat();
            client.player.OnTheGround = currentBuffer.ReadBool();
        }

        public void HandleSpawnPlayer(Client toSend, Client spawn)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x0C); //id of packet
            tmp.WriteVarInt(spawn.player.id);
            tmp.WriteUTF8String(spawn.player.id.ToString());
            tmp.WriteUTF8String(spawn.player.name);
            tmp.WriteInt((int)spawn.player.position.x * 32);
            tmp.WriteInt((int)spawn.player.position.y * 32);
            tmp.WriteInt((int)spawn.player.position.z * 32);
            tmp.WriteByte(GetYaw(spawn.player.rotX));
            tmp.WriteByte(GetPitch(spawn.player.rotY));
            tmp.WriteShort(0);
            tmp.WriteByte(0x66);
            tmp.WriteFloat(100.0f);
            tmp.WriteByte(0x7F);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            toSend.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        private byte GetYaw(float value)
        {
            return (byte)50;
        }

        private byte GetPitch(float value)
        {
            return (byte)90;
        }

        /* Entities movement */

        public void HandleEntities()
        {
            List<Entity> entities = client.clientManager.server.entityList;
            foreach (Entity e in entities)
            {
                /*    if (e.prevPosition != e.position)
                    {
                        Vector3 offset = new Vector3(e.position.x - e.prevPosition.x,
                                                     e.position.y - e.prevPosition.y,
                                                     e.position.z - e.prevPosition.z);
                        if (offset.x > 4 || offset.x < -4 ||
                            offset.y > 4 || offset.y < -4 ||
                            offset.z > 4 || offset.z < -4)
                            HandleEntityTeleport(e);
                        else if (e.prevRotation != e.rotation)
                                HandleEntityLookAndRelativeMove(e, offset);    
                        else
                                HandleEntityRelativeMove(e, offset);
                    }
                    else if (e.prevRotation != e.rotation)
                        HandleEntityLook(e);
                    else if (e.GetType() == typeof(Player))
                        HandleEntity(e);*/
                if (e.prevPosition != e.position ||
                    e.rotation != e.prevRotation)
                    HandleEntityTeleport(e);
            }
        }

        public void HandleEntityRelativeMove(Entity entity, Vector3 offset)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x15); //id of packet
            tmp.WriteInt(entity.id);
            tmp.WriteByte((byte)(offset.x * 32));
            tmp.WriteByte((byte)(offset.y * 32));
            tmp.WriteByte((byte)(offset.z * 32));

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            SendToClients(packet, entity);
        }

        public void HandleEntityLook(Entity entity)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x16); //id of packet
            tmp.WriteInt(entity.id);
            tmp.WriteInt((byte)((entity.rotation.x * 255) / 360));
            tmp.WriteInt((byte)((entity.rotation.y * 255) / 360));

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            SendToClients(packet, entity);
        }

        public void HandleEntityLookAndRelativeMove(Entity entity, Vector3 offset)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x17); //id of packet
            tmp.WriteInt(entity.id);
            tmp.WriteByte((byte)(offset.x * 32));
            tmp.WriteByte((byte)(offset.y * 32));
            tmp.WriteByte((byte)(offset.z * 32));
            tmp.WriteInt((byte)((entity.rotation.x * 255) / 360));
            tmp.WriteInt((byte)((entity.rotation.y * 255) / 360));

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            SendToClients(packet, entity);
        }

        public void HandleEntity(Entity entity)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x14); //id of packet
            tmp.WriteInt(entity.id);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            SendToClients(packet, entity);
        }

        public void HandleEntityTeleport(Entity entity)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x18); //id of packet
            tmp.WriteInt(entity.id);
            tmp.WriteInt((int)(entity.position.x * 32));
            tmp.WriteInt((int)(entity.position.y * 32));
            tmp.WriteInt((int)(entity.position.z * 32));
            tmp.WriteByte((byte)((entity.rotation.x * 255) / 360));
            tmp.WriteByte((byte)((entity.rotation.y * 255) / 360));

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            SendToClients(packet, entity);
        }

        private void SendToClients(BytesBuffer packet, Entity entity)
        {
            int excludeId = -1;
            List<Client> clientList = client.clientManager.clientList;

            lock (clientList)
            {
                if (entity.GetType() == typeof(Player))
                    excludeId = entity.id;
                foreach (Client cl in clientList)
                {
                    if (cl.nextState == 3 && cl.id != excludeId)
                        cl.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
                }
            }
        }

        private void HandlePlayerDigging(Client client, BytesBuffer currentBuffer)
        {
            Debug.WriteLine("[COMMAND] Player Digging");

            int StatusBloc = currentBuffer.ReadByte();
            int PosX = currentBuffer.ReadInt();
            byte PosY = (byte)currentBuffer.ReadByte();
            int PosZ = currentBuffer.ReadInt();
            int Face = currentBuffer.ReadByte();

            Debug.WriteLine("[COMAND] Status : " + StatusBloc);
            Debug.WriteLine("[COMAND] Pos X : " + PosX);
            Debug.WriteLine("[COMAND] Pos Y : " + PosY);
            Debug.WriteLine("[COMAND] Pos Z : " + PosZ);
            Debug.WriteLine("[COMAND] Face : " + Face);

            if (StatusBloc == 2 || (StatusBloc == 0 && client.clientManager.server.properties.gamemode == 1))
            {
                Debug.WriteLine("[COMMAND] Block Change");

                DeleteBlock(PosX, PosY, PosZ);
                BytesBuffer tmp = new BytesBuffer();

                tmp.WriteVarInt(0x23);
                tmp.WriteInt(PosX);
                tmp.WriteByte((byte)PosY);
                tmp.WriteInt(PosZ);
                tmp.WriteVarInt(0x00);
                tmp.WriteByte(0x00);

                BytesBuffer packet = new BytesBuffer();
                packet.WriteVarInt((int)tmp.Length);
                packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

                List<Client> clientList = client.clientManager.clientList;
                foreach (Client cl in clientList)
                {
                    if (cl.nextState == 3)
                        cl.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
                }
            }
        }

        private void DeleteBlock(int x, byte y, int z)
        {
            List<Chunk> chunkList = client.clientManager.server.chunkList;
            foreach (Chunk c in chunkList)
            {
                if ((x / 16) == c.x)
                {
                    if ((z / 16) == c.z)
                    {
                        Debug.WriteLine("c.Z =" + c.z + ",c.x = " + c.x);
                        int delete = (x - (16 * c.x)) + ((z - (16 * c.z)) * 16) + ((int)y * 256);
                        c.tableChunkData[delete] = (byte)0;
                    }
                }
            }
        }

        public void HandlePlayerDisconnect(Client client, int i)
        {
            BytesBuffer tmp = new BytesBuffer();
            int[] deleteEntity = new int[i];

            deleteEntity[0] = client.id;

            tmp.WriteVarInt(0x13);
            tmp.WriteByte((byte)i);
            tmp.WriteIntArray(deleteEntity);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        private void HandlePlayerBlockPlacement(Client client, BytesBuffer currentBuffer)
        {
            Debug.WriteLine("[COMMAND] Player Block Placement");

            int PosX = currentBuffer.ReadInt();
            byte PosY = (byte)(currentBuffer.ReadByte() + 1);
            int PosZ = currentBuffer.ReadInt();
            byte Direction = (byte)currentBuffer.ReadByte();
            if (PosX == -1 && (int)PosY == -1 && PosZ == -1 && (int)Direction == -1)
                return;
            int Slot = ReadSlot(currentBuffer);
            byte PosXVis = (byte)currentBuffer.ReadByte();
            byte PosYVis = (byte)currentBuffer.ReadByte();
            byte PosZVis = (byte)currentBuffer.ReadByte();

            AddBlock(PosX, PosY, PosZ, Slot);

            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x23);
            tmp.WriteInt(PosX);
            tmp.WriteByte((byte)PosY);
            tmp.WriteInt(PosZ);
            tmp.WriteVarInt(Slot);
            tmp.WriteByte(0x00);

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            List<Client> clientList = client.clientManager.clientList;
            foreach (Client cl in clientList)
            {
                if (cl.nextState == 3)
                    cl.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
            }
        }

        private int ReadSlot(BytesBuffer currentBuffer)
        {
            int id = currentBuffer.ReadShort();
            if (id >= 0)
            {
                byte count = (byte)currentBuffer.ReadByte();
                short len = currentBuffer.ReadShort();
                for (int i = 0; i < len; ++i)
                    currentBuffer.ReadByte();
            }
            return id;
        }

        private void AddBlock(int x, byte y, int z, int id)
        {
            List<Chunk> chunkList = client.clientManager.server.chunkList;
            foreach (Chunk c in chunkList)
            {
                if ((x / 16) == c.x)
                {
                    if ((z / 16) == c.z)
                    {
                        int add = (x - (16 * c.x)) + ((z - (16 * c.z)) * 16) + ((int)y * 256);
                        c.tableChunkData[add] = (byte)id;
                        break;
                    }
                }
            }
        }

        public void HandleDisconnect(string msg)
        {
            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteVarInt(0x40);
            tmp.WriteUTF8String("{\"text\": \"" + msg + "\"}");

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);
            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }
    }
}
