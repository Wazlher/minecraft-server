﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace DataServer
{
    class ServerPropertiesParser
    {
        private string path;
        private StreamReader sr;
        private Dictionary<string, string> lines;

        public ServerPropertiesParser(string _path)
        {
            path = _path;
            lines = new Dictionary<string, string>();
        }

        public bool Parse()
        {
            if (OpenFile())
            {
                string line = sr.ReadLine();
                while (sr.Peek() >= 0)
                {
                    if (line[0] == '#')
                    {
                        line = sr.ReadLine();
                        continue;
                    }
                    int index = line.IndexOf('=');
                    string arg = line.Substring(0, index);
                    string value = line.Substring(index + 1);
                    lines.Add(arg, value);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            else
                return false;
            return true;
        }

        private bool OpenFile()
        {
            try
            {
                sr = new StreamReader(path);
            }
            catch (Exception e)
            {
                Debug.WriteLine("[ERROR] Read the server properties file : " + e.ToString());
                return false;
            }
            return true;
        }

        public string GetValue(string arg, string value)
        {
            string ret;

            try
            {
                lines.TryGetValue(arg, out ret);
            }
            catch (System.ArgumentNullException e)
            {
                return value;
            }
            if (ret == null)
                return value;
            return ret;
        }
    }
}
