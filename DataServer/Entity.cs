﻿
namespace DataServer
{
    public class Entity
    {
        public int id { get; protected set; }

        public Vector3 position;
        public Vector3 prevPosition;

        public Vector3 rotation;
        public Vector3 prevRotation;

        public Entity(int _id)
        {
            id = _id;
            position = new Vector3(0.0, 100.0, 0.0);
            prevPosition = position;
            rotation = new Vector3(0.0, 0.0, 0.0);
            prevRotation = rotation;
        }
    }
}
