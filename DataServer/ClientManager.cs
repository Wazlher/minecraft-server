﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace DataServer
{
    public class ClientManager
    {
        public List<Client> clientList { get; private set; }
        private int maxId;
        public Server server;

        public ClientManager(Server s)
        {
            server = s;
            clientList = new List<Client>();
            maxId = -1;
        }

        public Client AddClient(TcpClient tcpClient)
        {
            int id = GetUniqueId();
            Client cl = new Client(tcpClient, id, this);
            clientList.Add(cl);
            return cl;
        }

        private int GetUniqueId()
        {
            clientList = clientList.OrderBy(n => n.id).ToList<Client>();
            for (int i = 0; i < maxId; ++i)
            {
                if (clientList[i].id != i)
                    return i;
            }
            maxId++;
            return maxId;
        }

        public List<Client> RemoveAllUsers()
        {
            List<Client> tmp;

            foreach (Client cl in clientList)
            {
                cl.toRemove = true;
                cl.command.HandleDisconnect("The server has been closed. Good bye :)");
                cl.Close();
            }
            tmp = clientList;
            clientList = new List<Client>();
            server.entityList = new List<Entity>();
            maxId = -1;
            return tmp;
        }

        public Client KickAClient(int id, bool stilConnected)
        {
            foreach (Client cl in clientList)
            {
                if (cl.id == id)
                {
                    if (stilConnected)
                        cl.command.HandleDisconnect("You have been kicked :o ");
                    Client tmp = cl;
                    tmp.toRemove = true;
                    cl.Close();
                    clientList.Remove(cl);
                    server.entityList.Remove(tmp.player);
                    if (tmp.id == maxId)
                        SearchGoodMaxId();
                    return tmp;
                }
            }
            return null;
        }

        private void SearchGoodMaxId()
        {
            maxId = -1;

            foreach (Client cl in clientList)
            {
                if (cl.id > maxId)
                    maxId = cl.id;
            }
        }
    }
}
