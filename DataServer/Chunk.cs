﻿using ICSharpCode.SharpZipLib.Zip.Compression;

namespace DataServer
{
    public class Chunk
    {
        public int x;
        public int z;
        public bool col;
        public ushort a;
        public ushort b;
        public int size;
        public byte[] tableChunkData;

        public static int botChunk = -6 * 16;
        public static int topChunk = 6 * 16;
        public static int leftChunk = -6 * 16;
        public static int rightChunk = 6 * 16;


        BytesBuffer parser;

        public void chunkData(int chunkX, int chunkZ)
        {
            x = chunkX;
            z = chunkZ;
            col = true;
            a = 0xffff;
            b = 0x00;
            size = 0;
        }

        public void initMapFormat(int type)
        {
            int x = 0;
            int i = 0;
            tableChunkData = new byte[164096];
            while (x < 65536) // ID
            {
                if (20479 < x && x < 22016)
                {
                    if (type == 0)
                        tableChunkData[i] = 0x00;
                    else if (type == 1)
                        tableChunkData[i] = 0x01;
                    else if (type == 2)
                        tableChunkData[i] = 0x02;
                    else if (type == 3)
                        tableChunkData[i] = 0x03;
                    else if (type == 4)
                        tableChunkData[i] = 0x07;
                    else if (type == 5)
                        tableChunkData[i] = 0x08;
                    else if (type == 6)
                        tableChunkData[i] = 0x0A;
                    else if (type == 7)
                        tableChunkData[i] = 0x0C;
                    else if (type == 8)
                        tableChunkData[i] = 0x0D;
                    else if (type == 9)
                        tableChunkData[i] = 0x0E;
                    else if (type == 10)
                        tableChunkData[i] = 0x0F;
                    else if (type == 11)
                        tableChunkData[i] = 0x15;
                    else if (type == 12)
                        tableChunkData[i] = 0x50;
                    else if (type == 13)
                        tableChunkData[i] = 0x1F;
                    else if (type == 14)
                        tableChunkData[i] = 0x25;
                    else if (type == 15)
                        tableChunkData[i] = 0x50;
                    else if (type == 16)
                        tableChunkData[i] = 0x28;
                    else
                        tableChunkData[i] = 0x53;
				}
				else
                    tableChunkData[i] = 0x00;
                x++;
                i++;
            }
            x = 0;
            while (x < 32768) // MetaData
            {
                tableChunkData[i] = 0x00;
                x++;
                i++;
            }
            x = 0;
            while (x < 32768) // illuminate
            {
                tableChunkData[i] = 0xFF;
                x++;
                i++;
            }
            x = 0;
            while (x < 32768) // illuminate
            {
                tableChunkData[i] = 0xFF;
                x++;
                i++;
            }
            x = 0;
            while (x < 256) // biome
            {
                tableChunkData[i] = 0x01;
                x++;
                i++;
            }
        }

        public void compressTableAndAdd(Client client)
        {
            byte[] tableCompressData = new byte[164096];
            Deflater deflater;
            deflater = new Deflater(5);
            deflater.SetInput(tableChunkData, 0, 164096);
            deflater.Finish();
            size = deflater.Deflate(tableCompressData);
            deflater.Reset();

            BytesBuffer tmp = new BytesBuffer();

            tmp.WriteByte(0x21);
            tmp.WriteInt(x);
            tmp.WriteInt(z);
            tmp.WriteBool(col);
            tmp.WriteLEUShort(a);
            tmp.WriteLEUShort(b);
            tmp.WriteInt(size);
            int i = 0;
            while (i < size)
            {
                tmp.WriteByte(tableCompressData[i]);
                i++;
            }

            BytesBuffer packet = new BytesBuffer();
            packet.WriteVarInt((int)tmp.Length);
            packet.Write(tmp.GetBuffer(), 0, (int)tmp.Length);

            client.stream.Write(packet.GetBuffer(), 0, (int)packet.Length);
        }

        public BytesBuffer getChunkBuffer()
        {
            return parser;
        }
    }
}
