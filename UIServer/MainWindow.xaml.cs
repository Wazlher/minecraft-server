﻿using DataServer;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace UIServer
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        Server server;

        private ObservableCollection<Info> _playerList;
        public ObservableCollection<Info> playerList
        {
            get { return this._playerList; }
            set { this._playerList = value; OnPropertyChanged("playerList"); }
        }

        private string _fullPath;
        public string fullPath
        {
            get { return this._fullPath; }
            set { this._fullPath = value; OnPropertyChanged("fullPath"); }
        }
        public string path = null;
        public string filename = null;

        #region Info/Debug variables
        private string _info;
        public string info
        {
            get { return this._info; }
            set { this._info = value; OnPropertyChanged("info"); }
        }
        private Brush _infoColor;
        public Brush infoColor
        {
            get { return this._infoColor; }
            set { this._infoColor = value; OnPropertyChanged("infoColor"); }
        }
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Loaded += Window_Loaded;
            Closing += Window_Closing;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            playerList = new ObservableCollection<Info>();
            server = new Server();
            server.ClientChange += Server_ClientChange;
            path = CheckServerPropertiesExist();
            if (path == null)
                fullPath = "Path  : No path selected ";
            else
                fullPath = "Path  :  " + path + filename;
            DataList.ItemsSource = playerList;
            ChangeInfoField("Server ready to be started", false);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (server != null && server.state != Server.ServerState.E_NOT_STARTED)
                server.Close();
        }

        private void Button_Remove(object sender, RoutedEventArgs e)
        {
            int id = -1;
            int indexSelected = DataList.SelectedIndex;
            if (indexSelected >= 0)
                id = playerList[indexSelected].id;
            if (id == -1)
                ChangeInfoField("You have to select a client", false);
            else if (!(id >= 0 && server.KickAClient(id, true)))
                ChangeInfoField("Client cannot be kick", true);
            else
                ChangeInfoField("Client has been kicked", false);
            Debug.WriteLine(indexSelected);
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            if (server != null && server.state == Server.ServerState.E_NOT_STARTED)
            {
                Debug.WriteLine("Play_Click");
                if (!server.Init(path + filename))
                {
                    ChangeInfoField("server.properties has not been found or could not be opened", true);
                    return;
                }
                if (!server.Start())
                {
                    ChangeInfoField("Server cannot start, port already in use", true);
                    return;
                }
                ChangeInfoField("Server has been started", false);
            }
            else
                ChangeInfoField("Server has already been started", false);
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (server != null && server.state != Server.ServerState.E_NOT_STARTED)
            {
                server.Close();
                ChangeInfoField("Server has been stopped", false);
            }
            else
                ChangeInfoField("Server must be started if you want to close it", false);
        }

        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            Stream stream;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            Nullable<bool> retOpen = null;

            openFileDialog.InitialDirectory = null;
            openFileDialog.Filter = "Server File (*.properties) | *.properties";
            openFileDialog.RestoreDirectory = true;

            retOpen = openFileDialog.ShowDialog();
            if (false == retOpen)
                return;

            try
            {
                if ((stream = openFileDialog.OpenFile()) != null)
                {
                    this.filename = openFileDialog.SafeFileName;
                    this.path = openFileDialog.FileName.Replace(filename, "");
                    if (path == null)
                        fullPath = "Path  : No path selected ";
                    else
                        fullPath = "Path  :  " + path + filename;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ERROR] Find server.properties file " + ex.Message);
                ChangeInfoField("server.properties file has not a valid path", true);
            }
            Debug.WriteLine("[INFO] New server.properties path = " + this.path);
            ChangeInfoField("server.properties change is ok", false);
        }

        public void Server_ClientChange(Client cl)
        {
            //Search for player in database
            if (cl.nextState == 0 || cl.nextState == 1)
                return;
            foreach (Info inf in playerList)
            {
                if (cl.id == inf.id)
                {
                    if (cl.toRemove)
                    {
                        DispatcherUtils.Dispatch(() =>
                            {
                                playerList.Remove(inf);
                            });
                    }
                    else
                        inf.Update(cl.player);
                    return;
                }
            }
            //If the player is not in the database, we add it
            DispatcherUtils.Dispatch(() =>
            {
                Info inf = new Info(cl);
                playerList.Add(inf);
            });
        }

        private string CheckServerPropertiesExist()
        {
            string file = Directory.GetCurrentDirectory() + "\\server.properties";
            if (File.Exists(file))
                return file;
            return null;
        }

        private void ChangeInfoField(string text, bool error)
        {
            info = text;
            if (error)
                infoColor = Brushes.Red;
            else
                infoColor = Brushes.Black;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
