﻿using System;
using System.Windows;

namespace UIServer
{
    static class DispatcherUtils
    {
        static public void Dispatch(Action act)
        {
            if (Application.Current.Dispatcher.CheckAccess())
            {
                act();
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(act);
            }
        }
    }
}
