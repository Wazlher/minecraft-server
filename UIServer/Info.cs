﻿using DataServer;
using System;
using System.ComponentModel;

namespace UIServer
{
    public class Info : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _name;
        public string name
        {
            get { return this._name; }
            set { this._name = value; OnPropertyChanged("name"); }
        }

        private Vector3 _position;
        public Vector3 position
        {
            get { return this._position; }
            set { this._position = value; OnPropertyChanged("position"); }
        }

        private int _id;
        public int id
        {
            get { return this._id; }
            set { this._id = value; OnPropertyChanged("id"); }
        }

        public Info()
        {
            name = "NoName";
            position = new Vector3(0.0, 0.0, 0.0);
            id = -1;
        }

        public Info(Client cl)
        {
            name = cl.player.name;
            position = new Vector3(cl.player.position.x, cl.player.position.y, cl.player.position.z);
            id = cl.id;
        }

        public void Update(Player p)
        {
            name = p.name;
            position = new Vector3(Math.Round(p.position.x), Math.Round(p.position.y), Math.Round(p.position.z));
        }

        private void OnPropertyChanged(string info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(info));
        }
    }
}
